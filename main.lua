-- love2d-bouncing Bouncing balls using love2d
-- Copyright (C) 2011  Ángel Alonso

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


function love.load()
    windowWidth = love.graphics.getWidth()
    windowHeight = love.graphics.getHeight()

    circles = {{100, 100, 300, 300}}
    radius = 10
end

function addCircle()
    table.insert(circles, {math.random(0, windowWidth),
        math.random(0, windowHeight), math.random(100, 500),
        math.random(100, 500)})
end

function love.keypressed(key)
    if key == 'a' then
        addCircle()
    end
end

function love.update(dt)
    for index,circle in ipairs(circles) do
        circle[1] = circle[1] + circle[3] * dt
        circle[2] = circle[2] + circle[4] * dt
        if circle[1] < radius then
            circle[1] = radius
            circle[3] = -circle[3]
        elseif circle[1] > (windowWidth - radius) then
            circle[1] = windowWidth - radius
            circle[3] = -circle[3]
        end
        if circle[2] < radius then
            circle[2] = radius
            circle[4] = -circle[4]
        elseif circle[2] > (windowHeight - radius) then
            circle[2] = windowHeight - radius
            circle[4] = -circle[4]
        end
    end
end

function love.draw()
    for index,circle in ipairs(circles) do
        love.graphics.circle("fill", circle[1], circle[2], radius)
    end
    love.graphics.print(love.timer.getFPS(), 0, 0)
end
